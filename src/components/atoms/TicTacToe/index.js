import React, { memo, useMemo, useCallback } from "react";
import styles from "./styles";

import { View, TouchableOpacity, Alert, Text } from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { useLocalStore } from "react-global-hook";
export const TicTacToe = memo(() => {
  const [{ gameState, currentPlayer }, actions] = useLocalStore(
    () => ({
      gameState: [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
      currentPlayer: 1,
    }),
    ({ setState, getState }) => ({
      handleGameState: (gameState) => {
        setState({ gameState });
      },
      handleCurrentPlayer: (currentPlayer) => {
        setState({ currentPlayer });
      },
    })
  );

  const handleinstialState = useCallback(() => {
    actions.handleGameState([[0, 0, 0], [0, 0, 0], [0, 0, 0]]);
    actions.handleCurrentPlayer(1);
  });

  const handleRenderIcon = useCallback((row, col) => {
    if (gameState) {
      var value = gameState[row][col];
      switch (value) {
        case 1:
          return <MaterialCommunityIcons name="close" size={80} color="red" />;
        case -1:
          return (
            <MaterialCommunityIcons
              name="circle-outline"
              size={80}
              color="green"
            />
          );
        default:
          return <View />;
      }
    }
  });
  const getWinner = useCallback(() => {
    const NUM_COLUMNS = 3;
    let arr = gameState;
    let sum;

    for (let i = 0; i < NUM_COLUMNS; i++) {
      sum = arr[i][0] + arr[i][1] + arr[i][2];
      if (sum == 3) return 1;
      else if (sum == -3) return -1;
    }
    for (let i = 0; i < NUM_COLUMNS; i++) {
      sum = arr[0][i] + arr[1][i] + arr[2][i];
      if (sum == 3) return 1;
      else if (sum == -3) return -1;
    }

    sum = arr[0][0] + arr[1][1] + arr[2][2];
    if (sum == 3) return 1;
    else if (sum == -3) return -1;

    sum = arr[0][2] + arr[1][1] + arr[2][0];
    if (sum == 3) return 1;
    else if (sum == -3) return -1;

    return 0;
  });
  const onTilePress = useCallback((row, col) => {
    if (gameState) {
      let value = gameState[row][col];
      if (value != 0) return;
    }

    let arr = gameState.slice();
    let currentPlayers = currentPlayer;
    arr[row][col] = currentPlayers;
    actions.handleGameState(arr);

    const nextPlayer = currentPlayer == 1 ? -1 : 1;
    actions.handleCurrentPlayer(nextPlayer);

    const winner = getWinner();
    if (winner == 1) {
      Alert.alert("اعلام برنده", "بازیکن شماره یک برنده شد");
      handleinstialState();
    } else if (winner == -1) {
      Alert.alert("اعلام برنده", "بازیکن شماره دو برنده شد");
      handleinstialState();
    }
  });
  useMemo(() => {
    handleinstialState();
  }, []);
  return (
    <View style={styles.container}>
      <View style={styles.tileContainer}>
        <TouchableOpacity
          onPress={() => onTilePress(0, 0)}
          style={[styles.tile, styles.noTopBorder, styles.noLeftBorder]}
        >
          {handleRenderIcon(0, 0)}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => onTilePress(0, 1)}
          style={[styles.tile, styles.noTopBorder]}
        >
          {handleRenderIcon(0, 1)}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => onTilePress(0, 2)}
          style={[styles.tile, styles.noRightBorder, styles.noTopBorder]}
        >
          {handleRenderIcon(0, 2)}
        </TouchableOpacity>
      </View>
      <View style={styles.tileContainer}>
        <TouchableOpacity
          onPress={() => onTilePress(1, 0)}
          style={[styles.tile, styles.noLeftBorder]}
        >
          {handleRenderIcon(1, 0)}
        </TouchableOpacity>
        <TouchableOpacity onPress={() => onTilePress(1, 1)} style={styles.tile}>
          {handleRenderIcon(1, 1)}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => onTilePress(1, 2)}
          style={[styles.tile, styles.noRightBorder]}
        >
          {handleRenderIcon(1, 2)}
        </TouchableOpacity>
      </View>
      <View style={styles.tileContainer}>
        <TouchableOpacity
          onPress={() => onTilePress(2, 0)}
          style={[styles.tile, styles.noBottomBorder, styles.noLeftBorder]}
        >
          {handleRenderIcon(2, 0)}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => onTilePress(2, 1)}
          style={[styles.tile, styles.noBottomBorder]}
        >
          {handleRenderIcon(2, 1)}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => onTilePress(2, 2)}
          style={[styles.tile, styles.noBottomBorder, styles.noRightBorder]}
        >
          {handleRenderIcon(2, 2)}
        </TouchableOpacity>
      </View>

      <TouchableOpacity style={{ marginTop: 80 }} onPress={handleinstialState}>
        <Text>New Game</Text>
      </TouchableOpacity>
    </View>
  );
});
