import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tile: {
    borderWidth: 1,
    width: 100,
    height: 100,
    justifyContent: 'center',
    alignItems: 'center'
  },
  tileContainer: {
    flexDirection: 'row',
  },
  noRightBorder: {
    borderRightWidth: 0,
  },
  noLeftBorder: {
    borderLeftWidth: 0,
  },
  noBottomBorder: {
    borderBottomWidth: 0,
  },
  noTopBorder: {
    borderTopWidth: 0,
  },
});
