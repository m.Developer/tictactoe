import React from "react";

import { View } from "react-native";
import { TicTacToe } from "../../atoms/";
import styles from "./styles";

const Third = () => {
  return (
    <View style={styles.container}>
      <TicTacToe />
    </View>
  );
};

export default Third;
