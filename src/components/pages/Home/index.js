import React from "react";

import { View } from "react-native";
import styles from "./styles";
import { TicTacToe } from "../../atoms/";

const Home = () => {
  return (
    <View style={styles.container}>
      <TicTacToe />
    </View>
  );
};

export default Home;
