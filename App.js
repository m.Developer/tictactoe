import React from "react";

import { NavigationContainer } from "@react-navigation/native";

import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import Home from "./src/components/pages/Home";
import Secound from "./src/components/pages/Secound";
import Third from "./src/components/pages/Third";
import { View } from "react-native";

const Tab = createMaterialTopTabNavigator();
const App = () => {
  return (
    <NavigationContainer>
      <Tab.Navigator tabBar={() => <View />}>
        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="Secound" component={Secound} />
        <Tab.Screen name="Third" component={Third} />
      </Tab.Navigator>
    </NavigationContainer>
  );
};

export default App;
