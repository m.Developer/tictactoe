# React Native [iOS + Android]

## 99% code sharing between iOS and Android

## TOC

- [demo](#demo)
- [install](#install)
- [run](#run)
- [build](#build)

## demo

<img src="/demo/firstPage.png"  width="200" height="400">
<img src="/demo/secoundpage.png"  width="200" height="400">
<img src="/demo/thirdpage.png"  width="200" height="400">

## install

- get clone of project

## run

- run
  - `$ npx react-native run-[android|ios]`
- [iOS] Via Xcode
  - `open ios/funzi.xcodeproj` (open the project on Xcode)
  - Press the Run button
- [Android] Via Android Studio
  - `studio android/` (open the project on Android Studio)
  - Press the Run button

## build

- android
  - `$ ./gradlew assembleRelease`
  - `$ npx react-native run-android --variant=release`

## Issuses

- react-native-gesture-handler:compileDebugJavaWithJavac FAILED
  - `$ npx jetify`

## Author

Email: [m.heydari.developer@gmail.com](email:m.heydari.developer@gmail.com)<br/>
